# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable      = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking.hostName = "nixos"; # Define your hostname.
  # Pick only one of the below networking options.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.
  # Easiest to use and most distros use this by default:
  networking.networkmanager = {
    enable  = true;
    plugins = [ pkgs.networkmanager-openvpn ];
  };

  # Set your time zone.
  time.timeZone = "America/New_York";

  # Desktop Environment.
  services.xserver = {
    enable = true;
    dpi = 168; # 75% increase
    libinput = {
      enable = true;
      # disabling mouse acceleration
      #mouse.accelProfile = "flat";
      # disabling touchpad acceleration
      #touchpad.accelProfile = "flat";
    };
    displayManager.defaultSession = "none+xmonad";
    windowManager = {
      xmonad.enable                 = true;
      xmonad.enableContribAndExtras = true;
      xmonad.extraPackages          = hpkgs: [
        hpkgs.xmonad
        hpkgs.xmonad-contrib
        hpkgs.xmonad-extras
      ];
    };
  };
  services.picom = {
    enable          = true;
    activeOpacity   = 0.95;
    inactiveOpacity = 0.85;
    backend         = "glx";
    fade            = true;
    fadeDelta       = 5;
    ## fails due to variable: opacityRule = [ "100:name *= 'i3lock'" ];
    shadow          = true;
    shadowOpacity   = 0.85;
  };

  # Enable CUPS to print documents.
  # services.printing.enable = true;

  # Enable sound.
  sound.enable               = true;
  hardware.pulseaudio.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.jc = {
    isNormalUser = true;
    extraGroups  = [ "wheel" ]; # Enable ‘sudo’ for the user.
  #   packages   = with pkgs; [
  #     firefox
  #     thunderbird
  #   ];
  };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    ## dev
    emacs
    git
    vim
    ## fonts
    fira-code
    ## gui
    firefox
    ## system
    alacritty # terminal
    htop
    killall
    pingtcp
    unzip
    wget
    zip
    ## window manager support
    clipman # clipboard manager
    dmenu # run launcher
    # dunst # notification system
    networkmanager_dmenu # manage networkmanager
    nitrogen # set wallpaper
    pcmanfm # file manager
    picom # compositor (?)
    trayer # system tray
    volumeicon # volume control
    xmobar # panel / dock
    xterm # just-in-case terminal
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # Copy the NixOS configuration file and link it from the resulting system
  # (/run/current-system/configuration.nix). This is useful in case you
  # accidentally delete configuration.nix.
  system.copySystemConfiguration = true;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "22.05"; # Did you read the comment?
}

